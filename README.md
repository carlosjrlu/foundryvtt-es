# FoundryVTT ES

Complete Translation to Spanish of the Foundry Virtual Tabletop

Thanks to Simone Ricciardi for the base files

## ¡¡¡IMPORTANTE!!!

Direcciones de instalación (manifest) de las distintas versiones:

* Última versión: https://gitlab.com/carlosjrlu/foundryvtt-es/-/raw/master/es/module.json
* v0.8.8: https://gitlab.com/carlosjrlu/foundryvtt-es/uploads/49fc6a8ea6934ae8258f048fec4113f3/module.json
* v0.8.7: https://gitlab.com/carlosjrlu/foundryvtt-es/uploads/2e69223edfec8ec413f07cbe0055b01e/module.json
* v0.8.6: https://gitlab.com/carlosjrlu/foundryvtt-es/uploads/9def857e68c8bae83d3290171f2c7809/module.json
* v0.7.10: https://gitlab.com/carlosjrlu/foundryvtt-es/uploads/a1d0647cc8c4fae69bdadd26cec74ab3/module.json

Si estás en la 0.7 deberás usar el manifest de la 0.7.10

## Español

Este módulo permite seleccionar el idioma *Español* en [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop").
Incluye la traducción completa de la interfaz.

Si encuentra algún error, o tiene alguna recomendación, háganoslo saber enviando un mensaje a **Cosmo Corban#4840** en el discord official de FoundryVTT (https://discord.gg/foundryvtt) o bien a **Viriato139ac#0342** en el discord de FoundryVTT Español (https://discord.gg/MHCerwd).

### Instalación

En la opción de "Add-On Modules" en el menú principal del programa, has clic en "Install Module" y escribe lo siguiente en la ventana que te aparece:

https://gitlab.com/carlosjrlu/foundryvtt-es/-/raw/master/es/module.json

Si no te funciona, puedes intentar descargar el archivo [es.zip](https://gitlab.com/carlosjrlu/foundryvtt-es/-/jobs/artifacts/master/raw/es.zip?job=build "es.zip") y extrae su contenido a la carpeta "Data\modules" y renombra el directorio a FoundryVTT-ES.

Además, dentro de tu mundo de juego, deberás habilitar el módulo, y luego cambiar el idioma en la configuración general del programa.



## English

This module allows to choose the *Spanish* in [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop").
Includes the complete translation of the program interface.

I don't know programming, neither how gitlab works, but I would like to know if you have any recommendation or if you find any errors in the translation. So please, let me know with a message to my discord **Cosmo Corban#4840** through the FoundryVTT discord server.

### Installation

In the 'Add-On Modules' tab of the main menu, click on 'Install Module' and write down this to the pop-up window:

https://gitlab.com/carlosjrlu/foundryvtt-es/-/raw/master/es/module.json

If that doesn't work, you could try downloading the file [es.zip](https://gitlab.com/carlosjrlu/foundryvtt-es/-/jobs/artifacts/master/raw/es.zip?job=build "es.zip") and unzip it in the 'Data\modules' folder and rename it to FoundryVTT-ES.

Also, you have to activate the module in your world, and then choose the language Spanish from the dropdown menu in the general settings.